﻿using System.Collections.Generic;

namespace SurveyApp.Api.ViewModel
{
    public class AnswerRequest
    {
        public string Username { get; set; }
        public int SurveyId { get; set; }

        public List<AnswerViewModel> Answers { get; set; }
    }

    public class AnswerViewModel
    {
        public int QuestionId { get; set; }
        public int QuestionOptionId { get; set; }
    }
}