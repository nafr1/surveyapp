﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using SurveyApp.Api.Infrastructure.EntityConfiguration;
using SurveyApp.Api.Model;

namespace SurveyApp.Api.Infrastructure
{
    public class SurveyAppContext : DbContext
    {
        public SurveyAppContext(DbContextOptions<SurveyAppContext> options) : base(options)
        {
        }

        public DbSet<Survey> Surveys { get; set; }
        public DbSet<QuestionType> QuestionTypes { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new AnswerEntityTypeConfiguration());
            builder.ApplyConfiguration(new QuestionEntityTypeConfiguration());
            builder.ApplyConfiguration(new QuestionOptionEntityTypeConfiguration());
            builder.ApplyConfiguration(new QuestionTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new SurveyEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserEntityTypeConfiguration());
        }
    }

    public class SurveyAppContextDesignFactory : IDesignTimeDbContextFactory<SurveyAppContext>
    {
        public SurveyAppContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SurveyAppContext>()
                .UseSqlServer("Server=.;Initial Catalog=SurveryAppDb;Integrated Security=true");

            return new SurveyAppContext(optionsBuilder.Options);
        }
    }
}