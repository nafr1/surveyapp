﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SurveyApp.Api.Model;

namespace SurveyApp.Api.Infrastructure.EntityConfiguration
{
    public class QuestionTypeEntityTypeConfiguration : IEntityTypeConfiguration<QuestionType>
    {
        public void Configure(EntityTypeBuilder<QuestionType> builder)
        {
            builder.ToTable("questiontypes");

            builder.HasKey(qt => qt.Id);

            builder.Property(qt => qt.Id)
                .ForSqlServerUseSequenceHiLo("surveyapp_questiontype_hilo")
                .IsRequired();

            builder.Property(qt => qt.Type)
                .HasMaxLength(200)
                .IsRequired();
        }
    }
}