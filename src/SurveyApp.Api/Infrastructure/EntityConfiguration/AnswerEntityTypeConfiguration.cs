﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SurveyApp.Api.Model;

namespace SurveyApp.Api.Infrastructure.EntityConfiguration
{
    public class AnswerEntityTypeConfiguration : IEntityTypeConfiguration<Answer>
    {
        public void Configure(EntityTypeBuilder<Answer> builder)
        {
            builder.ToTable("answers");

            builder.HasKey(a => a.Id);

            builder.Property(a => a.Id)
                .ForSqlServerUseSequenceHiLo("surveyapp_answers_hilo")
                .IsRequired();

            builder.Property(a => a.UserId).IsRequired();
            builder.Property(a => a.QuestionOptionId).IsRequired();
            builder.Property(a => a.QuestionId).IsRequired();

            builder.HasOne(a => a.User)
                .WithMany()
                .HasForeignKey(q => q.UserId);

            builder.HasOne(a => a.QuestionOption)
                .WithMany()
                .HasForeignKey(q => q.QuestionOptionId);
        }
    }
}