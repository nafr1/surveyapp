﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SurveyApp.Api.Model;

namespace SurveyApp.Api.Infrastructure.EntityConfiguration
{
    public class QuestionEntityTypeConfiguration : IEntityTypeConfiguration<Question>
    {
        public void Configure(EntityTypeBuilder<Question> builder)
        {
            builder.ToTable("questions");

            builder.HasKey(q => q.Id);

            builder.Property(q => q.Caption)
                .IsRequired()
                .HasMaxLength(500);

            builder.Property(q => q.QuestionTypeId).IsRequired();
            builder.Property(q => q.SurveyId).IsRequired();

            builder.HasMany(q => q.QuestionOptions)
                .WithOne()
                .HasForeignKey(o => o.QuestionId)
                .IsRequired();

            builder.HasOne(q => q.QuestionType)
                .WithMany()
                .HasForeignKey(q => q.QuestionTypeId);
        }
    }
}