﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SurveyApp.Api.Model;

namespace SurveyApp.Api.Infrastructure.EntityConfiguration
{
    public class QuestionOptionEntityTypeConfiguration : IEntityTypeConfiguration<QuestionOption>
    {
        public void Configure(EntityTypeBuilder<QuestionOption> builder)
        {
            builder.ToTable("questionoptions");

            builder.HasKey(qo => qo.Id);

            builder.Property(qo => qo.Caption)
                .IsRequired()
                .HasMaxLength(500);
        }
    }
}