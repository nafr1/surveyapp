﻿using System;

namespace SurveyApp.Api.Infrastructure.Exceptions
{
    public class SurveyAppDomainException : Exception
    {
        public SurveyAppDomainException()
        { }

        public SurveyAppDomainException(string message)
            : base(message)
        { }

        public SurveyAppDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}