﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;
using SurveyApp.Api.Model;

namespace SurveyApp.Api.Infrastructure
{
    public class SurveyAppContextSeed
    {
        public async Task SeedAsync(SurveyAppContext context, IHostingEnvironment env, IOptions<SurveyAppSettings> settings, ILogger<SurveyAppContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(SurveyAppContextSeed));

            await policy.ExecuteAsync(async () =>
            {
                if (!context.QuestionTypes.Any())
                {
                    await context.QuestionTypes.AddRangeAsync(GetPreconfiguredQuestionTypes());

                    await context.SaveChangesAsync();
                }

                if (!context.Surveys.Any())
                {
                    await context.Surveys.AddRangeAsync(GetPreconfiguredSurveys());

                    await context.SaveChangesAsync();
                }
            });
        }

        private static IEnumerable<Survey> GetPreconfiguredSurveys()
        {
            return new List<Survey>
            {
                new Survey
                {
                    Name = "Gemalto Survey",
                    Questions = new List<Question>
                    {
                        new Question
                        {
                            Caption = "How likely is it that you would recommend this company to a friend or colleague?",
                            QuestionTypeId = 1,
                            QuestionOptions = new List<QuestionOption>
                            {
                                new QuestionOption {Caption = "0"},
                                new QuestionOption {Caption = "1"},
                                new QuestionOption {Caption = "2"},
                                new QuestionOption {Caption = "3"},
                                new QuestionOption {Caption = "4"},
                                new QuestionOption {Caption = "5"}
                            }
                        },
                        new Question
                        {
                            Caption = "Overall, how satisfied or dissatisfied are you with our company?",
                            QuestionTypeId = 1,
                            QuestionOptions = new List<QuestionOption>
                            {
                                new QuestionOption {Caption = "Very satisfied"},
                                new QuestionOption {Caption = "Somewhat Satisfied"},
                                new QuestionOption {Caption = "Neither satisfied or dissatisfied"},
                                new QuestionOption {Caption = "Somewhat dissatisfied"},
                                new QuestionOption {Caption = "Very dissatisfied"}
                            }
                        },
                        new Question
                        {
                            Caption = "Which of the following words would you use to describe our products? Select all that apply.",
                            QuestionTypeId = 2,
                            QuestionOptions = new List<QuestionOption>
                            {
                                new QuestionOption {Caption = "Reliable"},
                                new QuestionOption {Caption = "High quality"},
                                new QuestionOption {Caption = "Useful"},
                                new QuestionOption {Caption = "Overpriced"},
                                new QuestionOption {Caption = "Poor quality"},
                                new QuestionOption {Caption = "Unreliable"}
                            }
                        },
                        new Question
                        {
                            Caption = "How likely are you to purchase any of our products again?",
                            QuestionTypeId = 1,
                            QuestionOptions = new List<QuestionOption>
                            {
                                new QuestionOption {Caption = "Extremely likely"},
                                new QuestionOption {Caption = "Somewhat likely"},
                                new QuestionOption {Caption = "Not so likely"},
                                new QuestionOption {Caption = "Not at all likely"}
                            }
                        }
                    }
                }
            };
        }

        private IEnumerable<QuestionType> GetPreconfiguredQuestionTypes()
        {
            return new List<QuestionType>
            {
                new QuestionType { Type = "Select"},
                new QuestionType { Type = "MultiSelect" }
            };
        }

        private static Policy CreatePolicy(ILogger<SurveyAppContextSeed> logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>().
                WaitAndRetryAsync(
                    retries,
                    retry => TimeSpan.FromSeconds(5),
                    (exception, timeSpan, retry, ctx) =>
                    {
                        logger.LogTrace($"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
                    }
                );
        }
    }
}