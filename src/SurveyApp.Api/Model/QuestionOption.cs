﻿namespace SurveyApp.Api.Model
{
    public class QuestionOption
    {
        public int Id { get; set; }

        public string Caption { get; set; }

        public int QuestionId { get; set; }
    }
}