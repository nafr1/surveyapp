﻿namespace SurveyApp.Api.Model
{
    public class QuestionType
    {
        public int Id { get; set; }

        public string Type { get; set; }
    }
}