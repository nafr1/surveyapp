﻿namespace SurveyApp.Api.Model
{
    public class Answer
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int QuestionOptionId { get; set; }
        public QuestionOption QuestionOption { get; set; }

        public int QuestionId { get; set; }
    }
}