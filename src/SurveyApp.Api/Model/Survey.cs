﻿using System.Collections.Generic;

namespace SurveyApp.Api.Model
{
    public class Survey
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Question> Questions { get; set; }
    }
}