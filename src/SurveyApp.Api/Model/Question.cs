﻿using System.Collections.Generic;

namespace SurveyApp.Api.Model
{
    public class Question
    {
        public int Id { get; set; }

        public string Caption { get; set; }

        public int QuestionTypeId { get; set; }

        public QuestionType QuestionType { get; set; }

        public int SurveyId { get; set; }

        public List<QuestionOption> QuestionOptions { get; set; }
    }
}