﻿using Microsoft.AspNetCore.Mvc;

namespace SurveyApp.Api.Controllers
{
    public class HomeController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return new RedirectResult("~/swagger");
        }
    }
}