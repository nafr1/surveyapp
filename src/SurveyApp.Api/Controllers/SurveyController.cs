﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SurveyApp.Api.Infrastructure;
using SurveyApp.Api.Model;
using SurveyApp.Api.ViewModel;

namespace SurveyApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurveyController : ControllerBase
    {
        private readonly SurveyAppContext _context;

        public SurveyController(SurveyAppContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        [HttpGet]
        [Route("{id:int}")]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(Survey), (int) HttpStatusCode.OK)]
        public async Task<IActionResult> GetSurveyById(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var survey = await _context.Surveys
                .Include(s => s.Questions)
                    .ThenInclude(q => q.QuestionOptions)
                .Include(s => s.Questions)
                    .ThenInclude(q => q.QuestionType)
                .SingleOrDefaultAsync(s => s.Id == id);

            if (survey != null)
            {
                return Ok(survey);
            }

            return NotFound();
        }

        [Route("answers")]
        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        public async Task<IActionResult> SurveyAnswers([FromBody] AnswerRequest answersReq)
        {
            var user = new User {Name = answersReq.Username};
            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            foreach (var a in answersReq.Answers)
            {
                var answer = new Answer
                {
                    QuestionOptionId = a.QuestionOptionId,
                    QuestionId = a.QuestionId,
                    UserId = user.Id
                };

                _context.Answers.Add(answer);

                
            }

            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}